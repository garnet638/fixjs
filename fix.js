//Variables
var fs = require('fs');
var code = require("./code.js");
global.size = 0;

//I could've done this synchronously, but eh
function findFileSize(){
  fs.readFile("code.js", 'utf8', function(err, data) {
    script = data.split("\r");
    global.size = script.length;
  });
}
findFileSize();

console.log(global.size);

//Main function
function main(){
for (x = 0; x < global.size; x++){
  //Because why not?
  console.log(x);
  //Refresh the reference
  delete require.cache[require.resolve("./code.js")];
  code = require("./code.js");
  //Get the reader and data
  var reader = fs.readFileSync("code.js", 'utf8');
  var script = reader.split("\r\n");
  var varLine = script.indexOf("//*Variables") + 1;
  var codeLine = script.indexOf("//*Code") + 1;
  var endLine = script.indexOf("//*End") + 1;
  console.log(`varLine: ${varLine}`);
  console.log(`codeLine: ${codeLine}`);
  console.log(`endLine: ${endLine}`);
  console.log(`errorLine: ${global.errorLine}`);
  //This is where the party (and my migrane) starts
  try{
    if (global.errorLine == null) {} //Hey, if it works
    else{
      //Check for variables that aren't there
      if (global.errorMsg.indexOf("ReferenceError:") > -1) {
        let varNeedingChanging = global.errorMsg.split(" ")[1];
        console.log(`${varNeedingChanging} needs to be inserted on line ${varLine}`);
        script.splice(varLine, 0, "var " + varNeedingChanging + " = \"PLACEHOLDER VARIABLE\";");
        fs.writeFileSync('code.js', script.join("\r\n"), 'utf8');
      }
      //Now for something, can't remember what error this is exactly
      else if (global.errorMsg.indexOf("TypeError:") > -1){
        let reason = global.errorMsg.split(": ");
        let varNeedingChanging = global.errorMsg.split(" ")[1];
        varNeedingChanging = varNeedingChanging.split(".")[0];
        console.log(`There is invalid code at line ${global.errorLine}`);
        script.splice(global.errorLine-1, 0, "/// Invalid Code");
        script.splice(global.errorLine, 1, "// " + script[global.errorLine]);
        fs.writeFileSync('code.js', script.join("\r\n"), 'utf8');
      }
    }
  }
  catch (err) {}
  //Don't try this at home
  global.errorLine = null;
}
}
console.log("The code has been fixed!")

setTimeout(main, 1000);
