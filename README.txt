   _____    _                     _   _____
  |  ___|  |_|   _    _          | | | ____| 
  | |__     _   | \  / |         | | | |___
  |  __|   | |   \ \/ /          | | |___  |
  | |      | |   / /\ \   _   ___| |  ___| |
  |_|      |_|  |_/  \_| |_| |_____| |_____| (v1.0.0)

  A program by Gabriel Ch.
  
###
### INFORMATION
###

  Fix.JS is a simple, easy to use script that can fix issues with
your JavaScript code in an instant. It uses special markers and
recursion to tell where and when things go wrong, and how to fix
them. Below are instructions on how to use the code, and a sample
code with all the fixable issues in the current version.
  Keep in mind, Fix.JS is just an AI, so it may not be able to fix
issues to the proficiency a human can.

As of v1.0.0, Fix.JS can:
  -Fix declaration/reference errors
  -Fix type errors
  -Tell you (in an albeit bad way) if you have unbalanced brackets
  
Fix.JS can NOT do the following:
  -Fix syntax errors
  -Fix said unbalanced braces
  -Bake you a bean burrito (I know how much we all want it to)
  
Necessities:
  -Knowledge of JavaScript
  -Node.JS (https://nodejs.org/en/)
  -A brain

  
###  
### HOW TO USE
###

Using Fix.JS is simple.
First, enclose your code in the following format:
  try{
    //CODE
  }
  catch (err) {
    global.errorMsg = err.toString();
    global.errorLine = parseInt(err.stack.split(":")[3]);
  }
  
Secondly, insert the following statements where required
  //*Variables
    In front of your variable declarations
  //*Code
    In front of your code
  //*End
    At the end of your program

Third, change the requirement of line 2 in fix.js to the file you need to fix
Fourth, place fix.js in your code's root folder
Fifth, run the following command in a terminal in your code's root folder
  node fix.js
    
###
### SAMPLE CODE
###
#
# Below is some sample code, to demonstrate what Fix.JS can do.
#

try{
//*Variables
var potato = "Potato ";
var tomato = "Tomato ";
var one = 1;
//*Code
console.log(tomato);
console.log(taco);
var tacint = taco.parseInt();
//*End
}
catch (err) {
  global.errorMsg = err.toString();
  global.errorLine = parseInt(err.stack.split(":")[3]);
}
